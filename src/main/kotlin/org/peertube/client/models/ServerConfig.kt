/**
* PeerTube
* # Introduction  The PeerTube API is built on HTTP(S). Our API is RESTful. It has predictable resource URLs. It returns HTTP response codes to indicate errors. It also accepts and returns JSON in the HTTP body. You can use your favorite HTTP/REST library for your programming language to use PeerTube. No official SDK is currently provided, but the spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 
*
* The version of the OpenAPI document: 2.1.0
* 
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
package org.peertube.client.models

import org.peertube.client.models.ServerConfigAutoBlacklist
import org.peertube.client.models.ServerConfigAvatar
import org.peertube.client.models.ServerConfigEmail
import org.peertube.client.models.ServerConfigImport
import org.peertube.client.models.ServerConfigInstance
import org.peertube.client.models.ServerConfigPlugin
import org.peertube.client.models.ServerConfigSignup
import org.peertube.client.models.ServerConfigTranscoding
import org.peertube.client.models.ServerConfigTrending
import org.peertube.client.models.ServerConfigUser
import org.peertube.client.models.ServerConfigVideo
import org.peertube.client.models.ServerConfigVideoCaption

import com.squareup.moshi.Json
/**
 * 
 * @param instance 
 * @param plugin 
 * @param theme 
 * @param email 
 * @param contactForm 
 * @param serverVersion 
 * @param serverCommit 
 * @param signup 
 * @param transcoding 
 * @param import 
 * @param autoBlacklist 
 * @param avatar 
 * @param video 
 * @param videoCaption 
 * @param user 
 * @param trending 
 * @param tracker 
 */

data class ServerConfig (
    @Json(name = "instance")
    val instance: ServerConfigInstance? = null,
    @Json(name = "plugin")
    val plugin: ServerConfigPlugin? = null,
    @Json(name = "theme")
    val theme: ServerConfigPlugin? = null,
    @Json(name = "email")
    val email: ServerConfigEmail? = null,
    @Json(name = "contactForm")
    val contactForm: ServerConfigEmail? = null,
    @Json(name = "serverVersion")
    val serverVersion: kotlin.String? = null,
    @Json(name = "serverCommit")
    val serverCommit: kotlin.String? = null,
    @Json(name = "signup")
    val signup: ServerConfigSignup? = null,
    @Json(name = "transcoding")
    val transcoding: ServerConfigTranscoding? = null,
    @Json(name = "import")
    val import: ServerConfigImport? = null,
    @Json(name = "autoBlacklist")
    val autoBlacklist: ServerConfigAutoBlacklist? = null,
    @Json(name = "avatar")
    val avatar: ServerConfigAvatar? = null,
    @Json(name = "video")
    val video: ServerConfigVideo? = null,
    @Json(name = "videoCaption")
    val videoCaption: ServerConfigVideoCaption? = null,
    @Json(name = "user")
    val user: ServerConfigUser? = null,
    @Json(name = "trending")
    val trending: ServerConfigTrending? = null,
    @Json(name = "tracker")
    val tracker: ServerConfigEmail? = null
) 



