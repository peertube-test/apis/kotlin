/**
* PeerTube
* # Introduction  The PeerTube API is built on HTTP(S). Our API is RESTful. It has predictable resource URLs. It returns HTTP response codes to indicate errors. It also accepts and returns JSON in the HTTP body. You can use your favorite HTTP/REST library for your programming language to use PeerTube. No official SDK is currently provided, but the spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ``` 
*
* The version of the OpenAPI document: 2.0.0
* 
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
package peertube-api.models

import peertube-api.models.VideoPlaylistOwnerAccount
import peertube-api.models.VideoPlaylistPrivacy

import com.squareup.moshi.Json
/**
 * 
 * @param id 
 * @param createdAt 
 * @param updatedAt 
 * @param description 
 * @param uuid 
 * @param displayName 
 * @param isLocal 
 * @param videoLength 
 * @param thumbnailPath 
 * @param privacy 
 * @param type 
 * @param ownerAccount 
 */

data class VideoPlaylist (
    @Json(name = "id")
    val id: java.math.BigDecimal? = null,
    @Json(name = "createdAt")
    val createdAt: kotlin.String? = null,
    @Json(name = "updatedAt")
    val updatedAt: kotlin.String? = null,
    @Json(name = "description")
    val description: kotlin.String? = null,
    @Json(name = "uuid")
    val uuid: kotlin.String? = null,
    @Json(name = "displayName")
    val displayName: kotlin.String? = null,
    @Json(name = "isLocal")
    val isLocal: kotlin.Boolean? = null,
    @Json(name = "videoLength")
    val videoLength: java.math.BigDecimal? = null,
    @Json(name = "thumbnailPath")
    val thumbnailPath: kotlin.String? = null,
    @Json(name = "privacy")
    val privacy: VideoPlaylistPrivacy? = null,
    @Json(name = "type")
    val type: VideoPlaylistPrivacy? = null,
    @Json(name = "ownerAccount")
    val ownerAccount: VideoPlaylistOwnerAccount? = null
) 



