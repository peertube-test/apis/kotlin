# Kotlin API client for PeerTube

## Requires

* Kotlin 1.3.41
* Gradle 4.9

## Build

First, create the gradle wrapper script:

```
gradle wrapper
```

Then, run:

```
./gradlew check assemble
```

This runs all tests and packages the library.

## Features/Implementation Notes

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *https://peertube.cpy.re/api/v1*. Change it when instanciating `ApiClient(basePath)`.

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**accountsGet**](docs/AccountsApi.md#accountsget) | **GET** /accounts | Get all accounts
*AccountsApi* | [**accountsNameGet**](docs/AccountsApi.md#accountsnameget) | **GET** /accounts/{name} | Get the account by name
*AccountsApi* | [**accountsNameVideosGet**](docs/AccountsApi.md#accountsnamevideosget) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account
*ConfigApi* | [**configAboutGet**](docs/ConfigApi.md#configaboutget) | **GET** /config/about | Get the instance about page content
*ConfigApi* | [**configCustomDelete**](docs/ConfigApi.md#configcustomdelete) | **DELETE** /config/custom | Delete the runtime configuration of the server
*ConfigApi* | [**configCustomGet**](docs/ConfigApi.md#configcustomget) | **GET** /config/custom | Get the runtime configuration of the server
*ConfigApi* | [**configCustomPut**](docs/ConfigApi.md#configcustomput) | **PUT** /config/custom | Set the runtime configuration of the server
*ConfigApi* | [**configGet**](docs/ConfigApi.md#configget) | **GET** /config | Get the public configuration of the server
*JobApi* | [**jobsStateGet**](docs/JobApi.md#jobsstateget) | **GET** /jobs/{state} | Get list of jobs
*MyUserApi* | [**usersMeAvatarPickPost**](docs/MyUserApi.md#usersmeavatarpickpost) | **POST** /users/me/avatar/pick | Update current user avatar
*MyUserApi* | [**usersMeGet**](docs/MyUserApi.md#usersmeget) | **GET** /users/me | Get current user information
*MyUserApi* | [**usersMePut**](docs/MyUserApi.md#usersmeput) | **PUT** /users/me | Update current user information
*MyUserApi* | [**usersMeSubscriptionsExistGet**](docs/MyUserApi.md#usersmesubscriptionsexistget) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for the current user
*MyUserApi* | [**usersMeSubscriptionsGet**](docs/MyUserApi.md#usersmesubscriptionsget) | **GET** /users/me/subscriptions | Get subscriptions of the current user
*MyUserApi* | [**usersMeSubscriptionsPost**](docs/MyUserApi.md#usersmesubscriptionspost) | **POST** /users/me/subscriptions | Add subscription to the current user
*MyUserApi* | [**usersMeSubscriptionsSubscriptionHandleDelete**](docs/MyUserApi.md#usersmesubscriptionssubscriptionhandledelete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of the current user for a given uri
*MyUserApi* | [**usersMeSubscriptionsSubscriptionHandleGet**](docs/MyUserApi.md#usersmesubscriptionssubscriptionhandleget) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of the current user for a given uri
*MyUserApi* | [**usersMeSubscriptionsVideosGet**](docs/MyUserApi.md#usersmesubscriptionsvideosget) | **GET** /users/me/subscriptions/videos | Get videos of subscriptions of the current user
*MyUserApi* | [**usersMeVideoQuotaUsedGet**](docs/MyUserApi.md#usersmevideoquotausedget) | **GET** /users/me/video-quota-used | Get current user used quota
*MyUserApi* | [**usersMeVideosGet**](docs/MyUserApi.md#usersmevideosget) | **GET** /users/me/videos | Get videos of the current user
*MyUserApi* | [**usersMeVideosImportsGet**](docs/MyUserApi.md#usersmevideosimportsget) | **GET** /users/me/videos/imports | Get video imports of current user
*MyUserApi* | [**usersMeVideosVideoIdRatingGet**](docs/MyUserApi.md#usersmevideosvideoidratingget) | **GET** /users/me/videos/{videoId}/rating | Get rating of video by its id, among those of the current user
*SearchApi* | [**searchVideosGet**](docs/SearchApi.md#searchvideosget) | **GET** /search/videos | Get the videos corresponding to a given query
*ServerFollowingApi* | [**serverFollowersGet**](docs/ServerFollowingApi.md#serverfollowersget) | **GET** /server/followers | Get followers of the server
*ServerFollowingApi* | [**serverFollowingGet**](docs/ServerFollowingApi.md#serverfollowingget) | **GET** /server/following | Get servers followed by the server
*ServerFollowingApi* | [**serverFollowingHostDelete**](docs/ServerFollowingApi.md#serverfollowinghostdelete) | **DELETE** /server/following/{host} | Unfollow a server by hostname
*ServerFollowingApi* | [**serverFollowingPost**](docs/ServerFollowingApi.md#serverfollowingpost) | **POST** /server/following | Follow a server
*UserApi* | [**accountsNameRatingsGet**](docs/UserApi.md#accountsnameratingsget) | **GET** /accounts/{name}/ratings | Get ratings of an account by its name
*UserApi* | [**usersGet**](docs/UserApi.md#usersget) | **GET** /users | Get a list of users
*UserApi* | [**usersIdDelete**](docs/UserApi.md#usersiddelete) | **DELETE** /users/{id} | Delete a user by its id
*UserApi* | [**usersIdGet**](docs/UserApi.md#usersidget) | **GET** /users/{id} | Get user by its id
*UserApi* | [**usersIdPut**](docs/UserApi.md#usersidput) | **PUT** /users/{id} | Update user profile by its id
*UserApi* | [**usersPost**](docs/UserApi.md#userspost) | **POST** /users | Creates user
*UserApi* | [**usersRegisterPost**](docs/UserApi.md#usersregisterpost) | **POST** /users/register | Register a user
*VideoApi* | [**accountsNameVideosGet**](docs/VideoApi.md#accountsnamevideosget) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account
*VideoApi* | [**videoChannelsChannelHandleVideosGet**](docs/VideoApi.md#videochannelschannelhandlevideosget) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
*VideoApi* | [**videosCategoriesGet**](docs/VideoApi.md#videoscategoriesget) | **GET** /videos/categories | Get list of video categories known by the server
*VideoApi* | [**videosGet**](docs/VideoApi.md#videosget) | **GET** /videos | Get list of videos
*VideoApi* | [**videosIdDelete**](docs/VideoApi.md#videosiddelete) | **DELETE** /videos/{id} | Delete a video by its id
*VideoApi* | [**videosIdDescriptionGet**](docs/VideoApi.md#videosiddescriptionget) | **GET** /videos/{id}/description | Get a video description by its id
*VideoApi* | [**videosIdGet**](docs/VideoApi.md#videosidget) | **GET** /videos/{id} | Get a video by its id
*VideoApi* | [**videosIdGiveOwnershipPost**](docs/VideoApi.md#videosidgiveownershippost) | **POST** /videos/{id}/give-ownership | Request change of ownership for a video you own, by its id
*VideoApi* | [**videosIdPut**](docs/VideoApi.md#videosidput) | **PUT** /videos/{id} | Update metadata for a video by its id
*VideoApi* | [**videosIdViewsPost**](docs/VideoApi.md#videosidviewspost) | **POST** /videos/{id}/views | Add a view to the video by its id
*VideoApi* | [**videosIdWatchingPut**](docs/VideoApi.md#videosidwatchingput) | **PUT** /videos/{id}/watching | Set watching progress of a video by its id for a user
*VideoApi* | [**videosImportsPost**](docs/VideoApi.md#videosimportspost) | **POST** /videos/imports | Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
*VideoApi* | [**videosLanguagesGet**](docs/VideoApi.md#videoslanguagesget) | **GET** /videos/languages | Get list of languages known by the server
*VideoApi* | [**videosLicencesGet**](docs/VideoApi.md#videoslicencesget) | **GET** /videos/licences | Get list of video licences known by the server
*VideoApi* | [**videosOwnershipGet**](docs/VideoApi.md#videosownershipget) | **GET** /videos/ownership | Get list of video ownership changes requests
*VideoApi* | [**videosOwnershipIdAcceptPost**](docs/VideoApi.md#videosownershipidacceptpost) | **POST** /videos/ownership/{id}/accept | Refuse ownership change request for video by its id
*VideoApi* | [**videosOwnershipIdRefusePost**](docs/VideoApi.md#videosownershipidrefusepost) | **POST** /videos/ownership/{id}/refuse | Accept ownership change request for video by its id
*VideoApi* | [**videosPrivaciesGet**](docs/VideoApi.md#videosprivaciesget) | **GET** /videos/privacies | Get list of privacy policies supported by the server
*VideoApi* | [**videosUploadPost**](docs/VideoApi.md#videosuploadpost) | **POST** /videos/upload | Upload a video file with its metadata
*VideoAbuseApi* | [**videosAbuseGet**](docs/VideoAbuseApi.md#videosabuseget) | **GET** /videos/abuse | Get list of reported video abuses
*VideoAbuseApi* | [**videosIdAbusePost**](docs/VideoAbuseApi.md#videosidabusepost) | **POST** /videos/{id}/abuse | Report an abuse, on a video by its id
*VideoBlacklistApi* | [**videosBlacklistGet**](docs/VideoBlacklistApi.md#videosblacklistget) | **GET** /videos/blacklist | Get list of videos on blacklist
*VideoBlacklistApi* | [**videosIdBlacklistDelete**](docs/VideoBlacklistApi.md#videosidblacklistdelete) | **DELETE** /videos/{id}/blacklist | Delete an entry of the blacklist of a video by its id
*VideoBlacklistApi* | [**videosIdBlacklistPost**](docs/VideoBlacklistApi.md#videosidblacklistpost) | **POST** /videos/{id}/blacklist | Put on blacklist a video by its id
*VideoCaptionApi* | [**videosIdCaptionsCaptionLanguageDelete**](docs/VideoCaptionApi.md#videosidcaptionscaptionlanguagedelete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
*VideoCaptionApi* | [**videosIdCaptionsCaptionLanguagePut**](docs/VideoCaptionApi.md#videosidcaptionscaptionlanguageput) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
*VideoCaptionApi* | [**videosIdCaptionsGet**](docs/VideoCaptionApi.md#videosidcaptionsget) | **GET** /videos/{id}/captions | Get list of video's captions
*VideoChannelApi* | [**accountsNameVideoChannelsGet**](docs/VideoChannelApi.md#accountsnamevideochannelsget) | **GET** /accounts/{name}/video-channels | Get video channels of an account by its name
*VideoChannelApi* | [**videoChannelsChannelHandleDelete**](docs/VideoChannelApi.md#videochannelschannelhandledelete) | **DELETE** /video-channels/{channelHandle} | Delete a video channel by its id
*VideoChannelApi* | [**videoChannelsChannelHandleGet**](docs/VideoChannelApi.md#videochannelschannelhandleget) | **GET** /video-channels/{channelHandle} | Get a video channel by its id
*VideoChannelApi* | [**videoChannelsChannelHandlePut**](docs/VideoChannelApi.md#videochannelschannelhandleput) | **PUT** /video-channels/{channelHandle} | Update a video channel by its id
*VideoChannelApi* | [**videoChannelsChannelHandleVideosGet**](docs/VideoChannelApi.md#videochannelschannelhandlevideosget) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
*VideoChannelApi* | [**videoChannelsGet**](docs/VideoChannelApi.md#videochannelsget) | **GET** /video-channels | Get list of video channels
*VideoChannelApi* | [**videoChannelsPost**](docs/VideoChannelApi.md#videochannelspost) | **POST** /video-channels | Creates a video channel for the current user
*VideoCommentApi* | [**videosIdCommentThreadsGet**](docs/VideoCommentApi.md#videosidcommentthreadsget) | **GET** /videos/{id}/comment-threads | Get the comment threads of a video by its id
*VideoCommentApi* | [**videosIdCommentThreadsPost**](docs/VideoCommentApi.md#videosidcommentthreadspost) | **POST** /videos/{id}/comment-threads | Creates a comment thread, on a video by its id
*VideoCommentApi* | [**videosIdCommentThreadsThreadIdGet**](docs/VideoCommentApi.md#videosidcommentthreadsthreadidget) | **GET** /videos/{id}/comment-threads/{threadId} | Get the comment thread by its id, of a video by its id
*VideoCommentApi* | [**videosIdCommentsCommentIdDelete**](docs/VideoCommentApi.md#videosidcommentscommentiddelete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment in a comment thread by its id, of a video by its id
*VideoCommentApi* | [**videosIdCommentsCommentIdPost**](docs/VideoCommentApi.md#videosidcommentscommentidpost) | **POST** /videos/{id}/comments/{commentId} | Creates a comment in a comment thread by its id, of a video by its id
*VideoPlaylistApi* | [**videoPlaylistsGet**](docs/VideoPlaylistApi.md#videoplaylistsget) | **GET** /video-playlists | Get list of video playlists
*VideoRateApi* | [**videosIdRatePut**](docs/VideoRateApi.md#videosidrateput) | **PUT** /videos/{id}/rate | Vote for a video by its id


<a name="documentation-for-models"></a>
## Documentation for Models

 - [org.peertube.client.models.Account](docs/Account.md)
 - [org.peertube.client.models.Actor](docs/Actor.md)
 - [org.peertube.client.models.AddUser](docs/AddUser.md)
 - [org.peertube.client.models.AddUserResponse](docs/AddUserResponse.md)
 - [org.peertube.client.models.Avatar](docs/Avatar.md)
 - [org.peertube.client.models.CommentThreadPostResponse](docs/CommentThreadPostResponse.md)
 - [org.peertube.client.models.CommentThreadResponse](docs/CommentThreadResponse.md)
 - [org.peertube.client.models.Follow](docs/Follow.md)
 - [org.peertube.client.models.GetMeVideoRating](docs/GetMeVideoRating.md)
 - [org.peertube.client.models.InlineObject](docs/InlineObject.md)
 - [org.peertube.client.models.InlineObject1](docs/InlineObject1.md)
 - [org.peertube.client.models.InlineObject2](docs/InlineObject2.md)
 - [org.peertube.client.models.InlineObject3](docs/InlineObject3.md)
 - [org.peertube.client.models.InlineObject4](docs/InlineObject4.md)
 - [org.peertube.client.models.InlineObject5](docs/InlineObject5.md)
 - [org.peertube.client.models.InlineResponse200](docs/InlineResponse200.md)
 - [org.peertube.client.models.Job](docs/Job.md)
 - [org.peertube.client.models.PlaylistElement](docs/PlaylistElement.md)
 - [org.peertube.client.models.RegisterUser](docs/RegisterUser.md)
 - [org.peertube.client.models.RegisterUserChannel](docs/RegisterUserChannel.md)
 - [org.peertube.client.models.ServerConfig](docs/ServerConfig.md)
 - [org.peertube.client.models.ServerConfigAbout](docs/ServerConfigAbout.md)
 - [org.peertube.client.models.ServerConfigAboutInstance](docs/ServerConfigAboutInstance.md)
 - [org.peertube.client.models.ServerConfigAutoBlacklist](docs/ServerConfigAutoBlacklist.md)
 - [org.peertube.client.models.ServerConfigAutoBlacklistVideos](docs/ServerConfigAutoBlacklistVideos.md)
 - [org.peertube.client.models.ServerConfigAvatar](docs/ServerConfigAvatar.md)
 - [org.peertube.client.models.ServerConfigAvatarFile](docs/ServerConfigAvatarFile.md)
 - [org.peertube.client.models.ServerConfigAvatarFileSize](docs/ServerConfigAvatarFileSize.md)
 - [org.peertube.client.models.ServerConfigCustom](docs/ServerConfigCustom.md)
 - [org.peertube.client.models.ServerConfigCustomAdmin](docs/ServerConfigCustomAdmin.md)
 - [org.peertube.client.models.ServerConfigCustomCache](docs/ServerConfigCustomCache.md)
 - [org.peertube.client.models.ServerConfigCustomCachePreviews](docs/ServerConfigCustomCachePreviews.md)
 - [org.peertube.client.models.ServerConfigCustomFollowers](docs/ServerConfigCustomFollowers.md)
 - [org.peertube.client.models.ServerConfigCustomFollowersInstance](docs/ServerConfigCustomFollowersInstance.md)
 - [org.peertube.client.models.ServerConfigCustomInstance](docs/ServerConfigCustomInstance.md)
 - [org.peertube.client.models.ServerConfigCustomServices](docs/ServerConfigCustomServices.md)
 - [org.peertube.client.models.ServerConfigCustomServicesTwitter](docs/ServerConfigCustomServicesTwitter.md)
 - [org.peertube.client.models.ServerConfigCustomSignup](docs/ServerConfigCustomSignup.md)
 - [org.peertube.client.models.ServerConfigCustomTheme](docs/ServerConfigCustomTheme.md)
 - [org.peertube.client.models.ServerConfigCustomTranscoding](docs/ServerConfigCustomTranscoding.md)
 - [org.peertube.client.models.ServerConfigCustomTranscodingResolutions](docs/ServerConfigCustomTranscodingResolutions.md)
 - [org.peertube.client.models.ServerConfigEmail](docs/ServerConfigEmail.md)
 - [org.peertube.client.models.ServerConfigImport](docs/ServerConfigImport.md)
 - [org.peertube.client.models.ServerConfigImportVideos](docs/ServerConfigImportVideos.md)
 - [org.peertube.client.models.ServerConfigInstance](docs/ServerConfigInstance.md)
 - [org.peertube.client.models.ServerConfigInstanceCustomizations](docs/ServerConfigInstanceCustomizations.md)
 - [org.peertube.client.models.ServerConfigPlugin](docs/ServerConfigPlugin.md)
 - [org.peertube.client.models.ServerConfigSignup](docs/ServerConfigSignup.md)
 - [org.peertube.client.models.ServerConfigTranscoding](docs/ServerConfigTranscoding.md)
 - [org.peertube.client.models.ServerConfigTrending](docs/ServerConfigTrending.md)
 - [org.peertube.client.models.ServerConfigTrendingVideos](docs/ServerConfigTrendingVideos.md)
 - [org.peertube.client.models.ServerConfigUser](docs/ServerConfigUser.md)
 - [org.peertube.client.models.ServerConfigVideo](docs/ServerConfigVideo.md)
 - [org.peertube.client.models.ServerConfigVideoCaption](docs/ServerConfigVideoCaption.md)
 - [org.peertube.client.models.ServerConfigVideoCaptionFile](docs/ServerConfigVideoCaptionFile.md)
 - [org.peertube.client.models.ServerConfigVideoFile](docs/ServerConfigVideoFile.md)
 - [org.peertube.client.models.ServerConfigVideoImage](docs/ServerConfigVideoImage.md)
 - [org.peertube.client.models.UpdateMe](docs/UpdateMe.md)
 - [org.peertube.client.models.UpdateUser](docs/UpdateUser.md)
 - [org.peertube.client.models.User](docs/User.md)
 - [org.peertube.client.models.UserWatchingVideo](docs/UserWatchingVideo.md)
 - [org.peertube.client.models.Video](docs/Video.md)
 - [org.peertube.client.models.VideoAbuse](docs/VideoAbuse.md)
 - [org.peertube.client.models.VideoAbuseVideo](docs/VideoAbuseVideo.md)
 - [org.peertube.client.models.VideoAccountSummary](docs/VideoAccountSummary.md)
 - [org.peertube.client.models.VideoBlacklist](docs/VideoBlacklist.md)
 - [org.peertube.client.models.VideoCaption](docs/VideoCaption.md)
 - [org.peertube.client.models.VideoChannel](docs/VideoChannel.md)
 - [org.peertube.client.models.VideoChannelCreate](docs/VideoChannelCreate.md)
 - [org.peertube.client.models.VideoChannelOwnerAccount](docs/VideoChannelOwnerAccount.md)
 - [org.peertube.client.models.VideoChannelSummary](docs/VideoChannelSummary.md)
 - [org.peertube.client.models.VideoChannelUpdate](docs/VideoChannelUpdate.md)
 - [org.peertube.client.models.VideoComment](docs/VideoComment.md)
 - [org.peertube.client.models.VideoCommentThreadTree](docs/VideoCommentThreadTree.md)
 - [org.peertube.client.models.VideoConstantNumber](docs/VideoConstantNumber.md)
 - [org.peertube.client.models.VideoConstantString](docs/VideoConstantString.md)
 - [org.peertube.client.models.VideoDetails](docs/VideoDetails.md)
 - [org.peertube.client.models.VideoDetailsAllOf](docs/VideoDetailsAllOf.md)
 - [org.peertube.client.models.VideoFile](docs/VideoFile.md)
 - [org.peertube.client.models.VideoImport](docs/VideoImport.md)
 - [org.peertube.client.models.VideoImportState](docs/VideoImportState.md)
 - [org.peertube.client.models.VideoImportStateConstant](docs/VideoImportStateConstant.md)
 - [org.peertube.client.models.VideoListResponse](docs/VideoListResponse.md)
 - [org.peertube.client.models.VideoPlaylist](docs/VideoPlaylist.md)
 - [org.peertube.client.models.VideoPlaylistOwnerAccount](docs/VideoPlaylistOwnerAccount.md)
 - [org.peertube.client.models.VideoPlaylistPrivacy](docs/VideoPlaylistPrivacy.md)
 - [org.peertube.client.models.VideoPrivacyConstant](docs/VideoPrivacyConstant.md)
 - [org.peertube.client.models.VideoPrivacySet](docs/VideoPrivacySet.md)
 - [org.peertube.client.models.VideoRating](docs/VideoRating.md)
 - [org.peertube.client.models.VideoResolutionConstant](docs/VideoResolutionConstant.md)
 - [org.peertube.client.models.VideoScheduledUpdate](docs/VideoScheduledUpdate.md)
 - [org.peertube.client.models.VideoStateConstant](docs/VideoStateConstant.md)
 - [org.peertube.client.models.VideoStreamingPlaylists](docs/VideoStreamingPlaylists.md)
 - [org.peertube.client.models.VideoStreamingPlaylistsRedundancies](docs/VideoStreamingPlaylistsRedundancies.md)
 - [org.peertube.client.models.VideoUploadResponse](docs/VideoUploadResponse.md)
 - [org.peertube.client.models.VideoUserHistory](docs/VideoUserHistory.md)


<a name="documentation-for-authorization"></a>
## Documentation for Authorization

<a name="OAuth2"></a>
### OAuth2

- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: 
  - admin: Admin scope
  - moderator: Moderator scope
  - user: User scope


## License

Copyright (C) 2015-2020 PeerTube Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see http://www.gnu.org/licenses.
