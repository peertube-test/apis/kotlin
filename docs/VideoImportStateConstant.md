
# VideoImportStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**inline**](#IdEnum) | The video import state (Pending &#x3D; 1, Success &#x3D; 2, Failed &#x3D; 3) |  [optional]
**label** | **kotlin.String** |  |  [optional]


<a name="IdEnum"></a>
## Enum: id
Name | Value
---- | -----
id | 1, 2, 3



