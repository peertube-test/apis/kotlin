
# VideoDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**uuid** | **kotlin.String** |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**publishedAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**originallyPublishedAt** | **kotlin.String** |  |  [optional]
**category** | [**VideoConstantNumber**](VideoConstantNumber.md) |  |  [optional]
**licence** | [**VideoConstantNumber**](VideoConstantNumber.md) |  |  [optional]
**language** | [**VideoConstantString**](VideoConstantString.md) |  |  [optional]
**privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**duration** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**isLocal** | **kotlin.Boolean** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**thumbnailPath** | **kotlin.String** |  |  [optional]
**previewPath** | **kotlin.String** |  |  [optional]
**embedPath** | **kotlin.String** |  |  [optional]
**views** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**likes** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**dislikes** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**nsfw** | **kotlin.Boolean** |  |  [optional]
**waitTranscoding** | **kotlin.Boolean** |  |  [optional]
**state** | [**VideoStateConstant**](VideoStateConstant.md) |  |  [optional]
**scheduledUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  |  [optional]
**blacklisted** | **kotlin.Boolean** |  |  [optional]
**blacklistedReason** | **kotlin.String** |  |  [optional]
**account** | [**Account**](Account.md) |  |  [optional]
**channel** | [**VideoChannel**](VideoChannel.md) |  |  [optional]
**userHistory** | [**VideoUserHistory**](VideoUserHistory.md) |  |  [optional]
**descriptionPath** | **kotlin.String** |  |  [optional]
**support** | **kotlin.String** |  |  [optional]
**tags** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]
**files** | [**kotlin.Array&lt;VideoFile&gt;**](VideoFile.md) |  |  [optional]
**commentsEnabled** | **kotlin.Boolean** |  |  [optional]
**downloadEnabled** | **kotlin.Boolean** |  |  [optional]
**trackerUrls** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]
**streamingPlaylists** | [**kotlin.Array&lt;VideoStreamingPlaylists&gt;**](VideoStreamingPlaylists.md) |  |  [optional]



