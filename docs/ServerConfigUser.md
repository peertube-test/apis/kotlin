
# ServerConfigUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videoQuota** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**videoQuotaDaily** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]



