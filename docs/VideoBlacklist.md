
# VideoBlacklist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**videoId** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**uuid** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**duration** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**views** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**likes** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**dislikes** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**nsfw** | **kotlin.Boolean** |  |  [optional]



