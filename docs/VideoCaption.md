
# VideoCaption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | [**VideoConstantString**](VideoConstantString.md) |  |  [optional]
**captionPath** | **kotlin.String** |  |  [optional]



