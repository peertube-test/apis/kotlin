# SearchApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchVideosGet**](SearchApi.md#searchVideosGet) | **GET** /search/videos | Get the videos corresponding to a given query


<a name="searchVideosGet"></a>
# **searchVideosGet**
> VideoListResponse searchVideosGet(search, start, count, sort)

Get the videos corresponding to a given query

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = SearchApi()
val search : kotlin.String = search_example // kotlin.String | String to search
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.searchVideosGet(search, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling SearchApi#searchVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling SearchApi#searchVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **kotlin.String**| String to search |
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: -name, -duration, -createdAt, -publishedAt, -views, -likes, -match]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

