
# VideoImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**targetUrl** | **kotlin.String** |  |  [optional]
**magnetUri** | **kotlin.String** |  |  [optional]
**torrentName** | **kotlin.String** |  |  [optional]
**state** | [**VideoImportState**](VideoImportState.md) |  |  [optional]
**error** | **kotlin.String** |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**video** | [**Video**](Video.md) |  |  [optional]



