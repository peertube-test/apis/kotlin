
# VideoStreamingPlaylists

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**type** | [**inline**](#TypeEnum) | Playlist type (HLS &#x3D; 1) |  [optional]
**playlistUrl** | **kotlin.String** |  |  [optional]
**segmentsSha256Url** | **kotlin.String** |  |  [optional]
**redundancies** | [**kotlin.Array&lt;VideoStreamingPlaylistsRedundancies&gt;**](VideoStreamingPlaylistsRedundancies.md) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: type
Name | Value
---- | -----
type | 1



