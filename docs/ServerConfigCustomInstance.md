
# ServerConfigCustomInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  |  [optional]
**shortDescription** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**terms** | **kotlin.String** |  |  [optional]
**defaultClientRoute** | **kotlin.String** |  |  [optional]
**isNSFW** | **kotlin.Boolean** |  |  [optional]
**defaultNSFWPolicy** | **kotlin.String** |  |  [optional]
**customizations** | [**ServerConfigInstanceCustomizations**](ServerConfigInstanceCustomizations.md) |  |  [optional]



