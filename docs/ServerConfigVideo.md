
# ServerConfigVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image** | [**ServerConfigVideoImage**](ServerConfigVideoImage.md) |  |  [optional]
**file** | [**ServerConfigVideoFile**](ServerConfigVideoFile.md) |  |  [optional]



