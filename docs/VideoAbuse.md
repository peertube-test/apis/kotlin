
# VideoAbuse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**reason** | **kotlin.String** |  |  [optional]
**reporterAccount** | [**Account**](Account.md) |  |  [optional]
**video** | [**VideoAbuseVideo**](VideoAbuseVideo.md) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]



