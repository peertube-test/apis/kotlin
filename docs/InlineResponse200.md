
# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **kotlin.Int** |  |  [optional]
**data** | [**kotlin.Array&lt;VideoCaption&gt;**](VideoCaption.md) |  |  [optional]



