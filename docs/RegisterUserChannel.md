
# RegisterUserChannel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** | The default channel name |  [optional]
**displayName** | **kotlin.String** | The default channel display name |  [optional]



