
# InlineObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatarfile** | [**java.io.File**](java.io.File.md) | The file to upload. |  [optional]



