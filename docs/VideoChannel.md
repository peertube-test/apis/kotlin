
# VideoChannel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**isLocal** | **kotlin.Boolean** |  |  [optional]
**ownerAccount** | [**VideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  |  [optional]



