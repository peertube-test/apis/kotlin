
# Video

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**uuid** | **kotlin.String** |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**publishedAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**originallyPublishedAt** | **kotlin.String** |  |  [optional]
**category** | [**VideoConstantNumber**](VideoConstantNumber.md) |  |  [optional]
**licence** | [**VideoConstantNumber**](VideoConstantNumber.md) |  |  [optional]
**language** | [**VideoConstantString**](VideoConstantString.md) |  |  [optional]
**privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**duration** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**isLocal** | **kotlin.Boolean** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**thumbnailPath** | **kotlin.String** |  |  [optional]
**previewPath** | **kotlin.String** |  |  [optional]
**embedPath** | **kotlin.String** |  |  [optional]
**views** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**likes** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**dislikes** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**nsfw** | **kotlin.Boolean** |  |  [optional]
**waitTranscoding** | **kotlin.Boolean** |  |  [optional]
**state** | [**VideoStateConstant**](VideoStateConstant.md) |  |  [optional]
**scheduledUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  |  [optional]
**blacklisted** | **kotlin.Boolean** |  |  [optional]
**blacklistedReason** | **kotlin.String** |  |  [optional]
**account** | [**VideoAccountSummary**](VideoAccountSummary.md) |  |  [optional]
**channel** | [**VideoChannelSummary**](VideoChannelSummary.md) |  |  [optional]
**userHistory** | [**VideoUserHistory**](VideoUserHistory.md) |  |  [optional]



