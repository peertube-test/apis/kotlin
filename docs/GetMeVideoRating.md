
# GetMeVideoRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | Id of the video  | 
**rating** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | Rating of the video  | 



