
# VideoDetailsAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descriptionPath** | **kotlin.String** |  |  [optional]
**support** | **kotlin.String** |  |  [optional]
**channel** | [**VideoChannel**](VideoChannel.md) |  |  [optional]
**account** | [**Account**](Account.md) |  |  [optional]
**tags** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]
**files** | [**kotlin.Array&lt;VideoFile&gt;**](VideoFile.md) |  |  [optional]
**commentsEnabled** | **kotlin.Boolean** |  |  [optional]
**downloadEnabled** | **kotlin.Boolean** |  |  [optional]
**trackerUrls** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]
**streamingPlaylists** | [**kotlin.Array&lt;VideoStreamingPlaylists&gt;**](VideoStreamingPlaylists.md) |  |  [optional]



