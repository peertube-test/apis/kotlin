
# VideoListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**data** | [**kotlin.Array&lt;Video&gt;**](Video.md) |  |  [optional]



