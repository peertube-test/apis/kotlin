
# ServerConfigCustomSignup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **kotlin.Boolean** |  |  [optional]
**limit** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**requiresEmailVerification** | **kotlin.Boolean** |  |  [optional]



