
# Avatar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **kotlin.String** |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]



