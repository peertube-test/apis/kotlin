
# VideoCommentThreadTree

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**VideoComment**](VideoComment.md) |  |  [optional]
**children** | [**kotlin.Array&lt;VideoCommentThreadTree&gt;**](VideoCommentThreadTree.md) |  |  [optional]



