
# VideoFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**magnetUri** | **kotlin.String** |  |  [optional]
**resolution** | [**VideoResolutionConstant**](VideoResolutionConstant.md) |  |  [optional]
**size** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | Video file size in bytes |  [optional]
**torrentUrl** | **kotlin.String** |  |  [optional]
**torrentDownloadUrl** | **kotlin.String** |  |  [optional]
**fileUrl** | **kotlin.String** |  |  [optional]
**fileDownloadUrl** | **kotlin.String** |  |  [optional]
**fps** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]



