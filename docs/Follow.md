
# Follow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**follower** | [**Actor**](Actor.md) |  |  [optional]
**following** | [**Actor**](Actor.md) |  |  [optional]
**score** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**state** | [**inline**](#StateEnum) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]


<a name="StateEnum"></a>
## Enum: state
Name | Value
---- | -----
state | pending, accepted



