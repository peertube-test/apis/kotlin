# VideoApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsNameVideosGet**](VideoApi.md#accountsNameVideosGet) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account
[**videoChannelsChannelHandleVideosGet**](VideoApi.md#videoChannelsChannelHandleVideosGet) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
[**videosCategoriesGet**](VideoApi.md#videosCategoriesGet) | **GET** /videos/categories | Get list of video categories known by the server
[**videosGet**](VideoApi.md#videosGet) | **GET** /videos | Get list of videos
[**videosIdDelete**](VideoApi.md#videosIdDelete) | **DELETE** /videos/{id} | Delete a video by its id
[**videosIdDescriptionGet**](VideoApi.md#videosIdDescriptionGet) | **GET** /videos/{id}/description | Get a video description by its id
[**videosIdGet**](VideoApi.md#videosIdGet) | **GET** /videos/{id} | Get a video by its id
[**videosIdGiveOwnershipPost**](VideoApi.md#videosIdGiveOwnershipPost) | **POST** /videos/{id}/give-ownership | Request change of ownership for a video you own, by its id
[**videosIdPut**](VideoApi.md#videosIdPut) | **PUT** /videos/{id} | Update metadata for a video by its id
[**videosIdViewsPost**](VideoApi.md#videosIdViewsPost) | **POST** /videos/{id}/views | Add a view to the video by its id
[**videosIdWatchingPut**](VideoApi.md#videosIdWatchingPut) | **PUT** /videos/{id}/watching | Set watching progress of a video by its id for a user
[**videosImportsPost**](VideoApi.md#videosImportsPost) | **POST** /videos/imports | Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
[**videosLanguagesGet**](VideoApi.md#videosLanguagesGet) | **GET** /videos/languages | Get list of languages known by the server
[**videosLicencesGet**](VideoApi.md#videosLicencesGet) | **GET** /videos/licences | Get list of video licences known by the server
[**videosOwnershipGet**](VideoApi.md#videosOwnershipGet) | **GET** /videos/ownership | Get list of video ownership changes requests
[**videosOwnershipIdAcceptPost**](VideoApi.md#videosOwnershipIdAcceptPost) | **POST** /videos/ownership/{id}/accept | Refuse ownership change request for video by its id
[**videosOwnershipIdRefusePost**](VideoApi.md#videosOwnershipIdRefusePost) | **POST** /videos/ownership/{id}/refuse | Accept ownership change request for video by its id
[**videosPrivaciesGet**](VideoApi.md#videosPrivaciesGet) | **GET** /videos/privacies | Get list of privacy policies supported by the server
[**videosUploadPost**](VideoApi.md#videosUploadPost) | **POST** /videos/upload | Upload a video file with its metadata


<a name="accountsNameVideosGet"></a>
# **accountsNameVideosGet**
> VideoListResponse accountsNameVideosGet(name)

Get videos for an account, provided the name of that account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val name : kotlin.String = name_example // kotlin.String | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)
try {
    val result : VideoListResponse = apiInstance.accountsNameVideosGet(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#accountsNameVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#accountsNameVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) |

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoChannelsChannelHandleVideosGet"></a>
# **videoChannelsChannelHandleVideosGet**
> VideoListResponse videoChannelsChannelHandleVideosGet(channelHandle)

Get videos of a video channel by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val channelHandle : kotlin.String = channelHandle_example // kotlin.String | The video channel handle (example: 'my_username@example.com' or 'my_username')
try {
    val result : VideoListResponse = apiInstance.videoChannelsChannelHandleVideosGet(channelHandle)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videoChannelsChannelHandleVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videoChannelsChannelHandleVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosCategoriesGet"></a>
# **videosCategoriesGet**
> kotlin.Array&lt;kotlin.String&gt; videosCategoriesGet()

Get list of video categories known by the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.Array<kotlin.String> = apiInstance.videosCategoriesGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosCategoriesGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosCategoriesGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.Array&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosGet"></a>
# **videosGet**
> VideoListResponse videosGet(categoryOneOf, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, start, count, sort)

Get list of videos

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val categoryOneOf : OneOfLessThanNumberCommaArrayGreaterThan =  // OneOfLessThanNumberCommaArrayGreaterThan | category id of the video
val tagsOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video
val tagsAllOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | tag(s) of the video, where all should be present in the video
val licenceOneOf : OneOfLessThanNumberCommaArrayGreaterThan =  // OneOfLessThanNumberCommaArrayGreaterThan | licence id of the video
val languageOneOf : OneOfLessThanStringCommaArrayGreaterThan =  // OneOfLessThanStringCommaArrayGreaterThan | language id of the video
val nsfw : kotlin.String = nsfw_example // kotlin.String | whether to include nsfw videos, if any
val filter : kotlin.String = filter_example // kotlin.String | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges) 
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort videos by criteria
try {
    val result : VideoListResponse = apiInstance.videosGet(categoryOneOf, tagsOneOf, tagsAllOf, licenceOneOf, languageOneOf, nsfw, filter, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryOneOf** | [**OneOfLessThanNumberCommaArrayGreaterThan**](.md)| category id of the video | [optional]
 **tagsOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video | [optional]
 **tagsAllOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| tag(s) of the video, where all should be present in the video | [optional]
 **licenceOneOf** | [**OneOfLessThanNumberCommaArrayGreaterThan**](.md)| licence id of the video | [optional]
 **languageOneOf** | [**OneOfLessThanStringCommaArrayGreaterThan**](.md)| language id of the video | [optional]
 **nsfw** | **kotlin.String**| whether to include nsfw videos, if any | [optional] [enum: true, false]
 **filter** | **kotlin.String**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] [enum: local, all-local]
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort videos by criteria | [optional] [enum: -name, -duration, -createdAt, -publishedAt, -views, -likes, -trending]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdDelete"></a>
# **videosIdDelete**
> videosIdDelete(id)

Delete a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosIdDelete(id)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdDescriptionGet"></a>
# **videosIdDescriptionGet**
> kotlin.String videosIdDescriptionGet(id)

Get a video description by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    val result : kotlin.String = apiInstance.videosIdDescriptionGet(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdDescriptionGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdDescriptionGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

**kotlin.String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdGet"></a>
# **videosIdGet**
> VideoDetails videosIdGet(id)

Get a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    val result : VideoDetails = apiInstance.videosIdGet(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdGiveOwnershipPost"></a>
# **videosIdGiveOwnershipPost**
> videosIdGiveOwnershipPost(id, username)

Request change of ownership for a video you own, by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val username : kotlin.String = username_example // kotlin.String | 
try {
    apiInstance.videosIdGiveOwnershipPost(id, username)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdGiveOwnershipPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdGiveOwnershipPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **username** | **kotlin.String**|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

<a name="videosIdPut"></a>
# **videosIdPut**
> videosIdPut(id, thumbnailfile, previewfile, category, licence, language, description, waitTranscoding, support, nsfw, name, tags, commentsEnabled, originallyPublishedAt, scheduleUpdate)

Update metadata for a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
val category : kotlin.String = category_example // kotlin.String | Video category
val licence : kotlin.String = licence_example // kotlin.String | Video licence
val language : kotlin.String = language_example // kotlin.String | Video language
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.String = waitTranscoding_example // kotlin.String | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | Text describing how to support the video uploader
val nsfw : kotlin.String = nsfw_example // kotlin.String | Whether or not this video contains sensitive content
val name : kotlin.String = name_example // kotlin.String | Video name
val tags : kotlin.Array<kotlin.String> = tags_example // kotlin.Array<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.String = commentsEnabled_example // kotlin.String | Enable or disable comments for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
try {
    apiInstance.videosIdPut(id, thumbnailfile, previewfile, category, licence, language, description, waitTranscoding, support, nsfw, name, tags, commentsEnabled, originallyPublishedAt, scheduleUpdate)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]
 **category** | **kotlin.String**| Video category | [optional]
 **licence** | **kotlin.String**| Video licence | [optional]
 **language** | **kotlin.String**| Video language | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.String**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| Text describing how to support the video uploader | [optional]
 **nsfw** | **kotlin.String**| Whether or not this video contains sensitive content | [optional]
 **name** | **kotlin.String**| Video name | [optional]
 **tags** | [**kotlin.Array&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.String**| Enable or disable comments for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

<a name="videosIdViewsPost"></a>
# **videosIdViewsPost**
> videosIdViewsPost(id)

Add a view to the video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosIdViewsPost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdViewsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdViewsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdWatchingPut"></a>
# **videosIdWatchingPut**
> videosIdWatchingPut(id, userWatchingVideo)

Set watching progress of a video by its id for a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val userWatchingVideo : UserWatchingVideo =  // UserWatchingVideo | 
try {
    apiInstance.videosIdWatchingPut(id, userWatchingVideo)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosIdWatchingPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosIdWatchingPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **userWatchingVideo** | [**UserWatchingVideo**](UserWatchingVideo.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="videosImportsPost"></a>
# **videosImportsPost**
> VideoUploadResponse videosImportsPost(channelId, name, torrentfile, targetUrl, magnetUri, thumbnailfile, previewfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, scheduleUpdate)

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val channelId : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Channel id that will contain this video
val name : kotlin.String = name_example // kotlin.String | Video name
val torrentfile : java.io.File = BINARY_DATA_HERE // java.io.File | Torrent File
val targetUrl : kotlin.String = targetUrl_example // kotlin.String | HTTP target URL
val magnetUri : kotlin.String = magnetUri_example // kotlin.String | Magnet URI
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.String = category_example // kotlin.String | Video category
val licence : kotlin.String = licence_example // kotlin.String | Video licence
val language : kotlin.String = language_example // kotlin.String | Video language
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.String = waitTranscoding_example // kotlin.String | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | Text describing how to support the video uploader
val nsfw : kotlin.String = nsfw_example // kotlin.String | Whether or not this video contains sensitive content
val tags : kotlin.Array<kotlin.String> = tags_example // kotlin.Array<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.String = commentsEnabled_example // kotlin.String | Enable or disable comments for this video
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
try {
    val result : VideoUploadResponse = apiInstance.videosImportsPost(channelId, name, torrentfile, targetUrl, magnetUri, thumbnailfile, previewfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, scheduleUpdate)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosImportsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosImportsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelId** | **java.math.BigDecimal**| Channel id that will contain this video |
 **name** | **kotlin.String**| Video name |
 **torrentfile** | **java.io.File**| Torrent File | [optional]
 **targetUrl** | **kotlin.String**| HTTP target URL | [optional]
 **magnetUri** | **kotlin.String**| Magnet URI | [optional]
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3]
 **category** | **kotlin.String**| Video category | [optional]
 **licence** | **kotlin.String**| Video licence | [optional]
 **language** | **kotlin.String**| Video language | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.String**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| Text describing how to support the video uploader | [optional]
 **nsfw** | **kotlin.String**| Whether or not this video contains sensitive content | [optional]
 **tags** | [**kotlin.Array&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.String**| Enable or disable comments for this video | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="videosLanguagesGet"></a>
# **videosLanguagesGet**
> kotlin.Array&lt;kotlin.String&gt; videosLanguagesGet()

Get list of languages known by the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.Array<kotlin.String> = apiInstance.videosLanguagesGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosLanguagesGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosLanguagesGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.Array&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosLicencesGet"></a>
# **videosLicencesGet**
> kotlin.Array&lt;kotlin.String&gt; videosLicencesGet()

Get list of video licences known by the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.Array<kotlin.String> = apiInstance.videosLicencesGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosLicencesGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosLicencesGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.Array&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosOwnershipGet"></a>
# **videosOwnershipGet**
> videosOwnershipGet()

Get list of video ownership changes requests

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    apiInstance.videosOwnershipGet()
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosOwnershipGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosOwnershipGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosOwnershipIdAcceptPost"></a>
# **videosOwnershipIdAcceptPost**
> videosOwnershipIdAcceptPost(id)

Refuse ownership change request for video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosOwnershipIdAcceptPost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosOwnershipIdAcceptPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosOwnershipIdAcceptPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosOwnershipIdRefusePost"></a>
# **videosOwnershipIdRefusePost**
> videosOwnershipIdRefusePost(id)

Accept ownership change request for video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosOwnershipIdRefusePost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosOwnershipIdRefusePost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosOwnershipIdRefusePost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosPrivaciesGet"></a>
# **videosPrivaciesGet**
> kotlin.Array&lt;kotlin.String&gt; videosPrivaciesGet()

Get list of privacy policies supported by the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
try {
    val result : kotlin.Array<kotlin.String> = apiInstance.videosPrivaciesGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosPrivaciesGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosPrivaciesGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**kotlin.Array&lt;kotlin.String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosUploadPost"></a>
# **videosUploadPost**
> VideoUploadResponse videosUploadPost(videofile, channelId, name, thumbnailfile, previewfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, originallyPublishedAt, scheduleUpdate)

Upload a video file with its metadata

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoApi()
val videofile : java.io.File = BINARY_DATA_HERE // java.io.File | Video file
val channelId : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Channel id that will contain this video
val name : kotlin.String = name_example // kotlin.String | Video name
val thumbnailfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video thumbnail file
val previewfile : java.io.File = BINARY_DATA_HERE // java.io.File | Video preview file
val privacy : VideoPrivacySet =  // VideoPrivacySet | 
val category : kotlin.String = category_example // kotlin.String | Video category
val licence : kotlin.String = licence_example // kotlin.String | Video licence
val language : kotlin.String = language_example // kotlin.String | Video language
val description : kotlin.String = description_example // kotlin.String | Video description
val waitTranscoding : kotlin.String = waitTranscoding_example // kotlin.String | Whether or not we wait transcoding before publish the video
val support : kotlin.String = support_example // kotlin.String | Text describing how to support the video uploader
val nsfw : kotlin.String = nsfw_example // kotlin.String | Whether or not this video contains sensitive content
val tags : kotlin.Array<kotlin.String> = tags_example // kotlin.Array<kotlin.String> | Video tags (maximum 5 tags each between 2 and 30 characters)
val commentsEnabled : kotlin.String = commentsEnabled_example // kotlin.String | Enable or disable comments for this video
val originallyPublishedAt : java.time.OffsetDateTime = 2013-10-20T19:20:30+01:00 // java.time.OffsetDateTime | Date when the content was originally published
val scheduleUpdate : VideoScheduledUpdate =  // VideoScheduledUpdate | 
try {
    val result : VideoUploadResponse = apiInstance.videosUploadPost(videofile, channelId, name, thumbnailfile, previewfile, privacy, category, licence, language, description, waitTranscoding, support, nsfw, tags, commentsEnabled, originallyPublishedAt, scheduleUpdate)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoApi#videosUploadPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoApi#videosUploadPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videofile** | **java.io.File**| Video file |
 **channelId** | **java.math.BigDecimal**| Channel id that will contain this video |
 **name** | **kotlin.String**| Video name |
 **thumbnailfile** | **java.io.File**| Video thumbnail file | [optional]
 **previewfile** | **java.io.File**| Video preview file | [optional]
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] [enum: 1, 2, 3]
 **category** | **kotlin.String**| Video category | [optional]
 **licence** | **kotlin.String**| Video licence | [optional]
 **language** | **kotlin.String**| Video language | [optional]
 **description** | **kotlin.String**| Video description | [optional]
 **waitTranscoding** | **kotlin.String**| Whether or not we wait transcoding before publish the video | [optional]
 **support** | **kotlin.String**| Text describing how to support the video uploader | [optional]
 **nsfw** | **kotlin.String**| Whether or not this video contains sensitive content | [optional]
 **tags** | [**kotlin.Array&lt;kotlin.String&gt;**](kotlin.String.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional]
 **commentsEnabled** | **kotlin.String**| Enable or disable comments for this video | [optional]
 **originallyPublishedAt** | **java.time.OffsetDateTime**| Date when the content was originally published | [optional]
 **scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional]

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

