
# Actor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**host** | **kotlin.String** |  |  [optional]
**followingCount** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**followersCount** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**avatar** | [**Avatar**](Avatar.md) |  |  [optional]



