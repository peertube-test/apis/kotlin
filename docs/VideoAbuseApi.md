# VideoAbuseApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosAbuseGet**](VideoAbuseApi.md#videosAbuseGet) | **GET** /videos/abuse | Get list of reported video abuses
[**videosIdAbusePost**](VideoAbuseApi.md#videosIdAbusePost) | **POST** /videos/{id}/abuse | Report an abuse, on a video by its id


<a name="videosAbuseGet"></a>
# **videosAbuseGet**
> kotlin.Array&lt;VideoAbuse&gt; videosAbuseGet(start, count, sort)

Get list of reported video abuses

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoAbuseApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort abuses by criteria
try {
    val result : kotlin.Array<VideoAbuse> = apiInstance.videosAbuseGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoAbuseApi#videosAbuseGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAbuseApi#videosAbuseGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort abuses by criteria | [optional] [enum: -id, -createdAt, -state]

### Return type

[**kotlin.Array&lt;VideoAbuse&gt;**](VideoAbuse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdAbusePost"></a>
# **videosIdAbusePost**
> videosIdAbusePost(id)

Report an abuse, on a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoAbuseApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosIdAbusePost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoAbuseApi#videosIdAbusePost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoAbuseApi#videosIdAbusePost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

