
# VideoUploadResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video** | [**VideoChannelOwnerAccount**](VideoChannelOwnerAccount.md) |  |  [optional]



