
# VideoComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**text** | **kotlin.String** |  |  [optional]
**threadId** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**inReplyToCommentId** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**videoId** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**totalRepliesFromVideoAuthor** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**totalReplies** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**account** | [**Account**](Account.md) |  |  [optional]



