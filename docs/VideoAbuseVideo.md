
# VideoAbuseVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**uuid** | **kotlin.String** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]



