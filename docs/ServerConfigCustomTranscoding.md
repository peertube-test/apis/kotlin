
# ServerConfigCustomTranscoding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **kotlin.Boolean** |  |  [optional]
**allowAdditionalExtensions** | **kotlin.Boolean** |  |  [optional]
**allowAudioFiles** | **kotlin.Boolean** |  |  [optional]
**threads** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**resolutions** | [**ServerConfigCustomTranscodingResolutions**](ServerConfigCustomTranscodingResolutions.md) |  |  [optional]
**hls** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]



