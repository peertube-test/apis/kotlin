
# AddUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **kotlin.String** | The user username  | 
**password** | **kotlin.String** | The user password  | 
**email** | **kotlin.String** | The user email  | 
**videoQuota** | **kotlin.String** | The user videoQuota  | 
**videoQuotaDaily** | **kotlin.String** | The user daily video quota  | 
**role** | [**inline**](#RoleEnum) | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | 


<a name="RoleEnum"></a>
## Enum: role
Name | Value
---- | -----
role | 0, 1, 2



