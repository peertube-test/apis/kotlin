
# PlaylistElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**startTimestamp** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**stopTimestamp** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**video** | [**Video**](Video.md) |  |  [optional]



