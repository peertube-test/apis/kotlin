
# ServerConfigVideoImage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extensions** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]
**size** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  |  [optional]



