
# VideoPrivacyConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**inline**](#IdEnum) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]


<a name="IdEnum"></a>
## Enum: id
Name | Value
---- | -----
id | 1, 2, 3



