# ConfigApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**configAboutGet**](ConfigApi.md#configAboutGet) | **GET** /config/about | Get the instance about page content
[**configCustomDelete**](ConfigApi.md#configCustomDelete) | **DELETE** /config/custom | Delete the runtime configuration of the server
[**configCustomGet**](ConfigApi.md#configCustomGet) | **GET** /config/custom | Get the runtime configuration of the server
[**configCustomPut**](ConfigApi.md#configCustomPut) | **PUT** /config/custom | Set the runtime configuration of the server
[**configGet**](ConfigApi.md#configGet) | **GET** /config | Get the public configuration of the server


<a name="configAboutGet"></a>
# **configAboutGet**
> ServerConfigAbout configAboutGet()

Get the instance about page content

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    val result : ServerConfigAbout = apiInstance.configAboutGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#configAboutGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#configAboutGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfigAbout**](ServerConfigAbout.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="configCustomDelete"></a>
# **configCustomDelete**
> configCustomDelete()

Delete the runtime configuration of the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    apiInstance.configCustomDelete()
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#configCustomDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#configCustomDelete")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="configCustomGet"></a>
# **configCustomGet**
> ServerConfigCustom configCustomGet()

Get the runtime configuration of the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    val result : ServerConfigCustom = apiInstance.configCustomGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#configCustomGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#configCustomGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfigCustom**](ServerConfigCustom.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="configCustomPut"></a>
# **configCustomPut**
> configCustomPut()

Set the runtime configuration of the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    apiInstance.configCustomPut()
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#configCustomPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#configCustomPut")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="configGet"></a>
# **configGet**
> ServerConfig configGet()

Get the public configuration of the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ConfigApi()
try {
    val result : ServerConfig = apiInstance.configGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ConfigApi#configGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ConfigApi#configGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerConfig**](ServerConfig.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

