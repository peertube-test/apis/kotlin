
# ServerConfigVideoCaptionFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | [**ServerConfigAvatarFileSize**](ServerConfigAvatarFileSize.md) |  |  [optional]
**extensions** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]



