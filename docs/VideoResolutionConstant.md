
# VideoResolutionConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.Int** | Video resolution (240, 360, 720 ...) |  [optional]
**label** | **kotlin.String** |  |  [optional]



