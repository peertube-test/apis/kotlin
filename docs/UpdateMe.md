
# UpdateMe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **kotlin.String** | Your new password  | 
**email** | **kotlin.String** | Your new email  | 
**displayNSFW** | **kotlin.String** | Your new displayNSFW  | 
**autoPlayVideo** | **kotlin.String** | Your new autoPlayVideo  | 



