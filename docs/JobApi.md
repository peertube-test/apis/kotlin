# JobApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**jobsStateGet**](JobApi.md#jobsStateGet) | **GET** /jobs/{state} | Get list of jobs


<a name="jobsStateGet"></a>
# **jobsStateGet**
> kotlin.Array&lt;Job&gt; jobsStateGet(state, start, count, sort)

Get list of jobs

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = JobApi()
val state : kotlin.String = state_example // kotlin.String | The state of the job
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : kotlin.Array<Job> = apiInstance.jobsStateGet(state, start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling JobApi#jobsStateGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling JobApi#jobsStateGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **kotlin.String**| The state of the job | [enum: active, completed, failed, waiting, delayed]
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**kotlin.Array&lt;Job&gt;**](Job.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

