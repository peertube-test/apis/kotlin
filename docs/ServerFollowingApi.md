# ServerFollowingApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serverFollowersGet**](ServerFollowingApi.md#serverFollowersGet) | **GET** /server/followers | Get followers of the server
[**serverFollowingGet**](ServerFollowingApi.md#serverFollowingGet) | **GET** /server/following | Get servers followed by the server
[**serverFollowingHostDelete**](ServerFollowingApi.md#serverFollowingHostDelete) | **DELETE** /server/following/{host} | Unfollow a server by hostname
[**serverFollowingPost**](ServerFollowingApi.md#serverFollowingPost) | **POST** /server/following | Follow a server


<a name="serverFollowersGet"></a>
# **serverFollowersGet**
> kotlin.Array&lt;Follow&gt; serverFollowersGet(start, count, sort)

Get followers of the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerFollowingApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : kotlin.Array<Follow> = apiInstance.serverFollowersGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ServerFollowingApi#serverFollowersGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerFollowingApi#serverFollowersGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**kotlin.Array&lt;Follow&gt;**](Follow.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="serverFollowingGet"></a>
# **serverFollowingGet**
> kotlin.Array&lt;Follow&gt; serverFollowingGet(start, count, sort)

Get servers followed by the server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerFollowingApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : kotlin.Array<Follow> = apiInstance.serverFollowingGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling ServerFollowingApi#serverFollowingGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerFollowingApi#serverFollowingGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**kotlin.Array&lt;Follow&gt;**](Follow.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="serverFollowingHostDelete"></a>
# **serverFollowingHostDelete**
> serverFollowingHostDelete(host)

Unfollow a server by hostname

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerFollowingApi()
val host : kotlin.String = host_example // kotlin.String | The host to unfollow 
try {
    apiInstance.serverFollowingHostDelete(host)
} catch (e: ClientException) {
    println("4xx response calling ServerFollowingApi#serverFollowingHostDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerFollowingApi#serverFollowingHostDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **kotlin.String**| The host to unfollow  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serverFollowingPost"></a>
# **serverFollowingPost**
> serverFollowingPost(follow)

Follow a server

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = ServerFollowingApi()
val follow : Follow =  // Follow | 
try {
    apiInstance.serverFollowingPost(follow)
} catch (e: ClientException) {
    println("4xx response calling ServerFollowingApi#serverFollowingPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling ServerFollowingApi#serverFollowingPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **follow** | [**Follow**](Follow.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

