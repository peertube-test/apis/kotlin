
# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**state** | [**inline**](#StateEnum) |  |  [optional]
**category** | [**inline**](#CategoryEnum) |  |  [optional]
**handlerName** | **kotlin.String** |  |  [optional]
**handlerInputData** | **kotlin.String** |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]


<a name="StateEnum"></a>
## Enum: state
Name | Value
---- | -----
state | pending, processing, error, success


<a name="CategoryEnum"></a>
## Enum: category
Name | Value
---- | -----
category | transcoding, activitypub-http



