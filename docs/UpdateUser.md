
# UpdateUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **kotlin.String** | The user id  | 
**email** | **kotlin.String** | The updated email of the user  | 
**videoQuota** | **kotlin.String** | The updated videoQuota of the user  | 
**videoQuotaDaily** | **kotlin.String** | The updated daily video quota of the user  | 
**role** | [**inline**](#RoleEnum) | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) | 


<a name="RoleEnum"></a>
## Enum: role
Name | Value
---- | -----
role | 0, 1, 2



