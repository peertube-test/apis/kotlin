
# ServerConfigCustomCache

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previews** | [**ServerConfigCustomCachePreviews**](ServerConfigCustomCachePreviews.md) |  |  [optional]
**captions** | [**ServerConfigCustomCachePreviews**](ServerConfigCustomCachePreviews.md) |  |  [optional]



