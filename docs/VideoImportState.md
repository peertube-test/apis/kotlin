
# VideoImportState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**VideoImportStateConstant**](VideoImportStateConstant.md) |  |  [optional]
**label** | **kotlin.String** |  |  [optional]



