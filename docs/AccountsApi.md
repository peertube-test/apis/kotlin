# AccountsApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsGet**](AccountsApi.md#accountsGet) | **GET** /accounts | Get all accounts
[**accountsNameGet**](AccountsApi.md#accountsNameGet) | **GET** /accounts/{name} | Get the account by name
[**accountsNameVideosGet**](AccountsApi.md#accountsNameVideosGet) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account


<a name="accountsGet"></a>
# **accountsGet**
> kotlin.Array&lt;Account&gt; accountsGet(start, count, sort)

Get all accounts

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : kotlin.Array<Account> = apiInstance.accountsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#accountsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#accountsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**kotlin.Array&lt;Account&gt;**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="accountsNameGet"></a>
# **accountsNameGet**
> Account accountsNameGet(name)

Get the account by name

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val name : kotlin.String = name_example // kotlin.String | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)
try {
    val result : Account = apiInstance.accountsNameGet(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#accountsNameGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#accountsNameGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) |

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="accountsNameVideosGet"></a>
# **accountsNameVideosGet**
> VideoListResponse accountsNameVideosGet(name)

Get videos for an account, provided the name of that account

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = AccountsApi()
val name : kotlin.String = name_example // kotlin.String | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)
try {
    val result : VideoListResponse = apiInstance.accountsNameVideosGet(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling AccountsApi#accountsNameVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling AccountsApi#accountsNameVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) |

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

