# VideoBlacklistApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosBlacklistGet**](VideoBlacklistApi.md#videosBlacklistGet) | **GET** /videos/blacklist | Get list of videos on blacklist
[**videosIdBlacklistDelete**](VideoBlacklistApi.md#videosIdBlacklistDelete) | **DELETE** /videos/{id}/blacklist | Delete an entry of the blacklist of a video by its id
[**videosIdBlacklistPost**](VideoBlacklistApi.md#videosIdBlacklistPost) | **POST** /videos/{id}/blacklist | Put on blacklist a video by its id


<a name="videosBlacklistGet"></a>
# **videosBlacklistGet**
> kotlin.Array&lt;VideoBlacklist&gt; videosBlacklistGet(start, count, sort)

Get list of videos on blacklist

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoBlacklistApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort blacklists by criteria
try {
    val result : kotlin.Array<VideoBlacklist> = apiInstance.videosBlacklistGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoBlacklistApi#videosBlacklistGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoBlacklistApi#videosBlacklistGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort blacklists by criteria | [optional] [enum: -id, -name, -duration, -views, -likes, -dislikes, -uuid, -createdAt]

### Return type

[**kotlin.Array&lt;VideoBlacklist&gt;**](VideoBlacklist.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videosIdBlacklistDelete"></a>
# **videosIdBlacklistDelete**
> videosIdBlacklistDelete(id)

Delete an entry of the blacklist of a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoBlacklistApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosIdBlacklistDelete(id)
} catch (e: ClientException) {
    println("4xx response calling VideoBlacklistApi#videosIdBlacklistDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoBlacklistApi#videosIdBlacklistDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdBlacklistPost"></a>
# **videosIdBlacklistPost**
> videosIdBlacklistPost(id)

Put on blacklist a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoBlacklistApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosIdBlacklistPost(id)
} catch (e: ClientException) {
    println("4xx response calling VideoBlacklistApi#videosIdBlacklistPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoBlacklistApi#videosIdBlacklistPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

