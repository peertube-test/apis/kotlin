# VideoRateApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosIdRatePut**](VideoRateApi.md#videosIdRatePut) | **PUT** /videos/{id}/rate | Vote for a video by its id


<a name="videosIdRatePut"></a>
# **videosIdRatePut**
> videosIdRatePut(id)

Vote for a video by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoRateApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    apiInstance.videosIdRatePut(id)
} catch (e: ClientException) {
    println("4xx response calling VideoRateApi#videosIdRatePut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoRateApi#videosIdRatePut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

