
# ServerConfigCustomTranscodingResolutions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**&#x60;240p&#x60;** | **kotlin.Boolean** |  |  [optional]
**&#x60;360p&#x60;** | **kotlin.Boolean** |  |  [optional]
**&#x60;480p&#x60;** | **kotlin.Boolean** |  |  [optional]
**&#x60;720p&#x60;** | **kotlin.Boolean** |  |  [optional]
**&#x60;1080p&#x60;** | **kotlin.Boolean** |  |  [optional]
**&#x60;2160p&#x60;** | **kotlin.Boolean** |  |  [optional]



