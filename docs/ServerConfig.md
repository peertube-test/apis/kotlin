
# ServerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigInstance**](ServerConfigInstance.md) |  |  [optional]
**plugin** | [**ServerConfigPlugin**](ServerConfigPlugin.md) |  |  [optional]
**theme** | [**ServerConfigPlugin**](ServerConfigPlugin.md) |  |  [optional]
**email** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]
**contactForm** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]
**serverVersion** | **kotlin.String** |  |  [optional]
**serverCommit** | **kotlin.String** |  |  [optional]
**signup** | [**ServerConfigSignup**](ServerConfigSignup.md) |  |  [optional]
**transcoding** | [**ServerConfigTranscoding**](ServerConfigTranscoding.md) |  |  [optional]
**import** | [**ServerConfigImport**](ServerConfigImport.md) |  |  [optional]
**autoBlacklist** | [**ServerConfigAutoBlacklist**](ServerConfigAutoBlacklist.md) |  |  [optional]
**avatar** | [**ServerConfigAvatar**](ServerConfigAvatar.md) |  |  [optional]
**video** | [**ServerConfigVideo**](ServerConfigVideo.md) |  |  [optional]
**videoCaption** | [**ServerConfigVideoCaption**](ServerConfigVideoCaption.md) |  |  [optional]
**user** | [**ServerConfigUser**](ServerConfigUser.md) |  |  [optional]
**trending** | [**ServerConfigTrending**](ServerConfigTrending.md) |  |  [optional]
**tracker** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]



