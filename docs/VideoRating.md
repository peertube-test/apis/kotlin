
# VideoRating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video** | [**Video**](Video.md) |  | 
**rating** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | Rating of the video | 



