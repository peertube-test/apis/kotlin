# VideoCaptionApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videosIdCaptionsCaptionLanguageDelete**](VideoCaptionApi.md#videosIdCaptionsCaptionLanguageDelete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**videosIdCaptionsCaptionLanguagePut**](VideoCaptionApi.md#videosIdCaptionsCaptionLanguagePut) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**videosIdCaptionsGet**](VideoCaptionApi.md#videosIdCaptionsGet) | **GET** /videos/{id}/captions | Get list of video&#39;s captions


<a name="videosIdCaptionsCaptionLanguageDelete"></a>
# **videosIdCaptionsCaptionLanguageDelete**
> videosIdCaptionsCaptionLanguageDelete(id, captionLanguage)

Delete a video caption

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCaptionApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val captionLanguage : kotlin.String = captionLanguage_example // kotlin.String | The caption language
try {
    apiInstance.videosIdCaptionsCaptionLanguageDelete(id, captionLanguage)
} catch (e: ClientException) {
    println("4xx response calling VideoCaptionApi#videosIdCaptionsCaptionLanguageDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCaptionApi#videosIdCaptionsCaptionLanguageDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **captionLanguage** | **kotlin.String**| The caption language |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videosIdCaptionsCaptionLanguagePut"></a>
# **videosIdCaptionsCaptionLanguagePut**
> videosIdCaptionsCaptionLanguagePut(id, captionLanguage, captionfile)

Add or replace a video caption

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCaptionApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
val captionLanguage : kotlin.String = captionLanguage_example // kotlin.String | The caption language
val captionfile : java.io.File = BINARY_DATA_HERE // java.io.File | The file to upload.
try {
    apiInstance.videosIdCaptionsCaptionLanguagePut(id, captionLanguage, captionfile)
} catch (e: ClientException) {
    println("4xx response calling VideoCaptionApi#videosIdCaptionsCaptionLanguagePut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCaptionApi#videosIdCaptionsCaptionLanguagePut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |
 **captionLanguage** | **kotlin.String**| The caption language |
 **captionfile** | **java.io.File**| The file to upload. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

<a name="videosIdCaptionsGet"></a>
# **videosIdCaptionsGet**
> InlineResponse200 videosIdCaptionsGet(id)

Get list of video&#39;s captions

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoCaptionApi()
val id : kotlin.String = id_example // kotlin.String | The video id or uuid
try {
    val result : InlineResponse200 = apiInstance.videosIdCaptionsGet(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoCaptionApi#videosIdCaptionsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoCaptionApi#videosIdCaptionsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **kotlin.String**| The video id or uuid |

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

