
# VideoAccountSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**name** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**url** | **kotlin.String** |  |  [optional]
**host** | **kotlin.String** |  |  [optional]
**avatar** | [**Avatar**](Avatar.md) |  |  [optional]



