
# InlineObject3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**videofile** | [**java.io.File**](java.io.File.md) | Video file | 
**channelId** | [**java.math.BigDecimal**](java.math.BigDecimal.md) | Channel id that will contain this video | 
**name** | **kotlin.String** | Video name | 
**thumbnailfile** | [**java.io.File**](java.io.File.md) | Video thumbnail file |  [optional]
**previewfile** | [**java.io.File**](java.io.File.md) | Video preview file |  [optional]
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  |  [optional]
**category** | **kotlin.String** | Video category |  [optional]
**licence** | **kotlin.String** | Video licence |  [optional]
**language** | **kotlin.String** | Video language |  [optional]
**description** | **kotlin.String** | Video description |  [optional]
**waitTranscoding** | **kotlin.String** | Whether or not we wait transcoding before publish the video |  [optional]
**support** | **kotlin.String** | Text describing how to support the video uploader |  [optional]
**nsfw** | **kotlin.String** | Whether or not this video contains sensitive content |  [optional]
**tags** | **kotlin.Array&lt;kotlin.String&gt;** | Video tags (maximum 5 tags each between 2 and 30 characters) |  [optional]
**commentsEnabled** | **kotlin.String** | Enable or disable comments for this video |  [optional]
**originallyPublishedAt** | [**java.time.OffsetDateTime**](java.time.OffsetDateTime.md) | Date when the content was originally published |  [optional]
**scheduleUpdate** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  |  [optional]



