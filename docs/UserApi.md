# UserApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsNameRatingsGet**](UserApi.md#accountsNameRatingsGet) | **GET** /accounts/{name}/ratings | Get ratings of an account by its name
[**usersGet**](UserApi.md#usersGet) | **GET** /users | Get a list of users
[**usersIdDelete**](UserApi.md#usersIdDelete) | **DELETE** /users/{id} | Delete a user by its id
[**usersIdGet**](UserApi.md#usersIdGet) | **GET** /users/{id} | Get user by its id
[**usersIdPut**](UserApi.md#usersIdPut) | **PUT** /users/{id} | Update user profile by its id
[**usersPost**](UserApi.md#usersPost) | **POST** /users | Creates user
[**usersRegisterPost**](UserApi.md#usersRegisterPost) | **POST** /users/register | Register a user


<a name="accountsNameRatingsGet"></a>
# **accountsNameRatingsGet**
> kotlin.Array&lt;VideoRating&gt; accountsNameRatingsGet(name, start, count, sort, rating)

Get ratings of an account by its name

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val name : kotlin.String = name_example // kotlin.String | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
val rating : kotlin.String = rating_example // kotlin.String | Optionally filter which ratings to retrieve
try {
    val result : kotlin.Array<VideoRating> = apiInstance.accountsNameRatingsGet(name, start, count, sort, rating)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#accountsNameRatingsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#accountsNameRatingsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) |
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]
 **rating** | **kotlin.String**| Optionally filter which ratings to retrieve | [optional] [enum: like, dislike]

### Return type

[**kotlin.Array&lt;VideoRating&gt;**](VideoRating.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersGet"></a>
# **usersGet**
> kotlin.Array&lt;User&gt; usersGet(start, count, sort)

Get a list of users

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort users by criteria
try {
    val result : kotlin.Array<User> = apiInstance.usersGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#usersGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#usersGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort users by criteria | [optional] [enum: -id, -username, -createdAt]

### Return type

[**kotlin.Array&lt;User&gt;**](User.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersIdDelete"></a>
# **usersIdDelete**
> usersIdDelete(id)

Delete a user by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val id : java.math.BigDecimal = 8.14 // java.math.BigDecimal | The user id
try {
    apiInstance.usersIdDelete(id)
} catch (e: ClientException) {
    println("4xx response calling UserApi#usersIdDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#usersIdDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **java.math.BigDecimal**| The user id |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersIdGet"></a>
# **usersIdGet**
> User usersIdGet(id)

Get user by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val id : java.math.BigDecimal = 8.14 // java.math.BigDecimal | The user id
try {
    val result : User = apiInstance.usersIdGet(id)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#usersIdGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#usersIdGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **java.math.BigDecimal**| The user id |

### Return type

[**User**](User.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersIdPut"></a>
# **usersIdPut**
> usersIdPut(id, updateUser)

Update user profile by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val id : java.math.BigDecimal = 8.14 // java.math.BigDecimal | The user id
val updateUser : UpdateUser =  // UpdateUser | 
try {
    apiInstance.usersIdPut(id, updateUser)
} catch (e: ClientException) {
    println("4xx response calling UserApi#usersIdPut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#usersIdPut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **java.math.BigDecimal**| The user id |
 **updateUser** | [**UpdateUser**](UpdateUser.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersPost"></a>
# **usersPost**
> AddUserResponse usersPost(addUser)

Creates user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val addUser : AddUser =  // AddUser | User to create
try {
    val result : AddUserResponse = apiInstance.usersPost(addUser)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling UserApi#usersPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#usersPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addUser** | [**AddUser**](AddUser.md)| User to create |

### Return type

[**AddUserResponse**](AddUserResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersRegisterPost"></a>
# **usersRegisterPost**
> usersRegisterPost(registerUser)

Register a user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = UserApi()
val registerUser : RegisterUser =  // RegisterUser | 
try {
    apiInstance.usersRegisterPost(registerUser)
} catch (e: ClientException) {
    println("4xx response calling UserApi#usersRegisterPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling UserApi#usersRegisterPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerUser** | [**RegisterUser**](RegisterUser.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

