
# ServerConfigTranscoding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hls** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]
**enabledResolutions** | [**kotlin.Array&lt;java.math.BigDecimal&gt;**](java.math.BigDecimal.md) |  |  [optional]



