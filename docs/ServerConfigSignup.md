
# ServerConfigSignup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allowed** | **kotlin.Boolean** |  |  [optional]
**allowedForCurrentIP** | **kotlin.Boolean** |  |  [optional]
**requiresEmailVerification** | **kotlin.Boolean** |  |  [optional]



