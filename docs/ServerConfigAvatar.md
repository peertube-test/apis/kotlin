
# ServerConfigAvatar

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**ServerConfigAvatarFile**](ServerConfigAvatarFile.md) |  |  [optional]
**extensions** | **kotlin.Array&lt;kotlin.String&gt;** |  |  [optional]



