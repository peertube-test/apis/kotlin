# MyUserApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersMeAvatarPickPost**](MyUserApi.md#usersMeAvatarPickPost) | **POST** /users/me/avatar/pick | Update current user avatar
[**usersMeGet**](MyUserApi.md#usersMeGet) | **GET** /users/me | Get current user information
[**usersMePut**](MyUserApi.md#usersMePut) | **PUT** /users/me | Update current user information
[**usersMeSubscriptionsExistGet**](MyUserApi.md#usersMeSubscriptionsExistGet) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for the current user
[**usersMeSubscriptionsGet**](MyUserApi.md#usersMeSubscriptionsGet) | **GET** /users/me/subscriptions | Get subscriptions of the current user
[**usersMeSubscriptionsPost**](MyUserApi.md#usersMeSubscriptionsPost) | **POST** /users/me/subscriptions | Add subscription to the current user
[**usersMeSubscriptionsSubscriptionHandleDelete**](MyUserApi.md#usersMeSubscriptionsSubscriptionHandleDelete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of the current user for a given uri
[**usersMeSubscriptionsSubscriptionHandleGet**](MyUserApi.md#usersMeSubscriptionsSubscriptionHandleGet) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of the current user for a given uri
[**usersMeSubscriptionsVideosGet**](MyUserApi.md#usersMeSubscriptionsVideosGet) | **GET** /users/me/subscriptions/videos | Get videos of subscriptions of the current user
[**usersMeVideoQuotaUsedGet**](MyUserApi.md#usersMeVideoQuotaUsedGet) | **GET** /users/me/video-quota-used | Get current user used quota
[**usersMeVideosGet**](MyUserApi.md#usersMeVideosGet) | **GET** /users/me/videos | Get videos of the current user
[**usersMeVideosImportsGet**](MyUserApi.md#usersMeVideosImportsGet) | **GET** /users/me/videos/imports | Get video imports of current user
[**usersMeVideosVideoIdRatingGet**](MyUserApi.md#usersMeVideosVideoIdRatingGet) | **GET** /users/me/videos/{videoId}/rating | Get rating of video by its id, among those of the current user


<a name="usersMeAvatarPickPost"></a>
# **usersMeAvatarPickPost**
> Avatar usersMeAvatarPickPost(avatarfile)

Update current user avatar

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val avatarfile : java.io.File = BINARY_DATA_HERE // java.io.File | The file to upload.
try {
    val result : Avatar = apiInstance.usersMeAvatarPickPost(avatarfile)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeAvatarPickPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeAvatarPickPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **avatarfile** | **java.io.File**| The file to upload. | [optional]

### Return type

[**Avatar**](Avatar.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="usersMeGet"></a>
# **usersMeGet**
> kotlin.Array&lt;User&gt; usersMeGet()

Get current user information

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
try {
    val result : kotlin.Array<User> = apiInstance.usersMeGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**kotlin.Array&lt;User&gt;**](User.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMePut"></a>
# **usersMePut**
> usersMePut(updateMe)

Update current user information

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val updateMe : UpdateMe =  // UpdateMe | 
try {
    apiInstance.usersMePut(updateMe)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMePut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMePut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateMe** | [**UpdateMe**](UpdateMe.md)|  |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="usersMeSubscriptionsExistGet"></a>
# **usersMeSubscriptionsExistGet**
> kotlin.Any usersMeSubscriptionsExistGet(uris)

Get if subscriptions exist for the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val uris : kotlin.Array<kotlin.String> =  // kotlin.Array<kotlin.String> | list of uris to check if each is part of the user subscriptions
try {
    val result : kotlin.Any = apiInstance.usersMeSubscriptionsExistGet(uris)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeSubscriptionsExistGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeSubscriptionsExistGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uris** | [**kotlin.Array&lt;kotlin.String&gt;**](kotlin.String.md)| list of uris to check if each is part of the user subscriptions |

### Return type

[**kotlin.Any**](kotlin.Any.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeSubscriptionsGet"></a>
# **usersMeSubscriptionsGet**
> usersMeSubscriptionsGet(start, count, sort)

Get subscriptions of the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    apiInstance.usersMeSubscriptionsGet(start, count, sort)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeSubscriptionsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeSubscriptionsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersMeSubscriptionsPost"></a>
# **usersMeSubscriptionsPost**
> usersMeSubscriptionsPost()

Add subscription to the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
try {
    apiInstance.usersMeSubscriptionsPost()
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeSubscriptionsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeSubscriptionsPost")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersMeSubscriptionsSubscriptionHandleDelete"></a>
# **usersMeSubscriptionsSubscriptionHandleDelete**
> usersMeSubscriptionsSubscriptionHandleDelete(subscriptionHandle)

Delete subscription of the current user for a given uri

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val subscriptionHandle : kotlin.String = subscriptionHandle_example // kotlin.String | The subscription handle (example: 'my_username@example.com' or 'my_username')
try {
    apiInstance.usersMeSubscriptionsSubscriptionHandleDelete(subscriptionHandle)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeSubscriptionsSubscriptionHandleDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeSubscriptionsSubscriptionHandleDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionHandle** | **kotlin.String**| The subscription handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="usersMeSubscriptionsSubscriptionHandleGet"></a>
# **usersMeSubscriptionsSubscriptionHandleGet**
> VideoChannel usersMeSubscriptionsSubscriptionHandleGet(subscriptionHandle)

Get subscription of the current user for a given uri

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val subscriptionHandle : kotlin.String = subscriptionHandle_example // kotlin.String | The subscription handle (example: 'my_username@example.com' or 'my_username')
try {
    val result : VideoChannel = apiInstance.usersMeSubscriptionsSubscriptionHandleGet(subscriptionHandle)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeSubscriptionsSubscriptionHandleGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeSubscriptionsSubscriptionHandleGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionHandle** | **kotlin.String**| The subscription handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeSubscriptionsVideosGet"></a>
# **usersMeSubscriptionsVideosGet**
> VideoListResponse usersMeSubscriptionsVideosGet(start, count, sort)

Get videos of subscriptions of the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : VideoListResponse = apiInstance.usersMeSubscriptionsVideosGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeSubscriptionsVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeSubscriptionsVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideoQuotaUsedGet"></a>
# **usersMeVideoQuotaUsedGet**
> java.math.BigDecimal usersMeVideoQuotaUsedGet()

Get current user used quota

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
try {
    val result : java.math.BigDecimal = apiInstance.usersMeVideoQuotaUsedGet()
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideoQuotaUsedGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideoQuotaUsedGet")
    e.printStackTrace()
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**java.math.BigDecimal**](java.math.BigDecimal.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosGet"></a>
# **usersMeVideosGet**
> VideoListResponse usersMeVideosGet(start, count, sort)

Get videos of the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : VideoListResponse = apiInstance.usersMeVideosGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosImportsGet"></a>
# **usersMeVideosImportsGet**
> VideoImport usersMeVideosImportsGet(start, count, sort)

Get video imports of current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : VideoImport = apiInstance.usersMeVideosImportsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideosImportsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideosImportsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**VideoImport**](VideoImport.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersMeVideosVideoIdRatingGet"></a>
# **usersMeVideosVideoIdRatingGet**
> GetMeVideoRating usersMeVideosVideoIdRatingGet(videoId)

Get rating of video by its id, among those of the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = MyUserApi()
val videoId : kotlin.String = videoId_example // kotlin.String | The video id 
try {
    val result : GetMeVideoRating = apiInstance.usersMeVideosVideoIdRatingGet(videoId)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling MyUserApi#usersMeVideosVideoIdRatingGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling MyUserApi#usersMeVideosVideoIdRatingGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoId** | **kotlin.String**| The video id  |

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

