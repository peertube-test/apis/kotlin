
# VideoStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**inline**](#IdEnum) | The video state (Published &#x3D; 1, to transcode &#x3D; 2, to import &#x3D; 3) |  [optional]
**label** | **kotlin.String** |  |  [optional]


<a name="IdEnum"></a>
## Enum: id
Name | Value
---- | -----
id | 1, 2, 3



