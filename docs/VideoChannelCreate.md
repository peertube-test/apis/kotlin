
# VideoChannelCreate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **kotlin.String** |  | 
**displayName** | **kotlin.String** |  | 
**description** | **kotlin.String** |  |  [optional]
**support** | **kotlin.String** |  |  [optional]



