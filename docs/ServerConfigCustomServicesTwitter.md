
# ServerConfigCustomServicesTwitter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **kotlin.String** |  |  [optional]
**whitelisted** | **kotlin.Boolean** |  |  [optional]



