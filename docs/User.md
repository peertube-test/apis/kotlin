
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**username** | **kotlin.String** |  |  [optional]
**email** | **kotlin.String** |  |  [optional]
**displayNSFW** | **kotlin.Boolean** |  |  [optional]
**autoPlayVideo** | **kotlin.Boolean** |  |  [optional]
**role** | [**inline**](#RoleEnum) | The user role (Admin &#x3D; 0, Moderator &#x3D; 1, User &#x3D; 2) |  [optional]
**roleLabel** | [**inline**](#RoleLabelEnum) |  |  [optional]
**videoQuota** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**videoQuotaDaily** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**account** | [**Account**](Account.md) |  |  [optional]
**videoChannels** | [**kotlin.Array&lt;VideoChannel&gt;**](VideoChannel.md) |  |  [optional]


<a name="RoleEnum"></a>
## Enum: role
Name | Value
---- | -----
role | 0, 1, 2


<a name="RoleLabelEnum"></a>
## Enum: roleLabel
Name | Value
---- | -----
roleLabel | User, Moderator, Administrator



