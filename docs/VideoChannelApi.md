# VideoChannelApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountsNameVideoChannelsGet**](VideoChannelApi.md#accountsNameVideoChannelsGet) | **GET** /accounts/{name}/video-channels | Get video channels of an account by its name
[**videoChannelsChannelHandleDelete**](VideoChannelApi.md#videoChannelsChannelHandleDelete) | **DELETE** /video-channels/{channelHandle} | Delete a video channel by its id
[**videoChannelsChannelHandleGet**](VideoChannelApi.md#videoChannelsChannelHandleGet) | **GET** /video-channels/{channelHandle} | Get a video channel by its id
[**videoChannelsChannelHandlePut**](VideoChannelApi.md#videoChannelsChannelHandlePut) | **PUT** /video-channels/{channelHandle} | Update a video channel by its id
[**videoChannelsChannelHandleVideosGet**](VideoChannelApi.md#videoChannelsChannelHandleVideosGet) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
[**videoChannelsGet**](VideoChannelApi.md#videoChannelsGet) | **GET** /video-channels | Get list of video channels
[**videoChannelsPost**](VideoChannelApi.md#videoChannelsPost) | **POST** /video-channels | Creates a video channel for the current user


<a name="accountsNameVideoChannelsGet"></a>
# **accountsNameVideoChannelsGet**
> kotlin.Array&lt;VideoChannel&gt; accountsNameVideoChannelsGet(name)

Get video channels of an account by its name

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val name : kotlin.String = name_example // kotlin.String | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)
try {
    val result : kotlin.Array<VideoChannel> = apiInstance.accountsNameVideoChannelsGet(name)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#accountsNameVideoChannelsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#accountsNameVideoChannelsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **kotlin.String**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) |

### Return type

[**kotlin.Array&lt;VideoChannel&gt;**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoChannelsChannelHandleDelete"></a>
# **videoChannelsChannelHandleDelete**
> videoChannelsChannelHandleDelete(channelHandle)

Delete a video channel by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val channelHandle : kotlin.String = channelHandle_example // kotlin.String | The video channel handle (example: 'my_username@example.com' or 'my_username')
try {
    apiInstance.videoChannelsChannelHandleDelete(channelHandle)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#videoChannelsChannelHandleDelete")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#videoChannelsChannelHandleDelete")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="videoChannelsChannelHandleGet"></a>
# **videoChannelsChannelHandleGet**
> VideoChannel videoChannelsChannelHandleGet(channelHandle)

Get a video channel by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val channelHandle : kotlin.String = channelHandle_example // kotlin.String | The video channel handle (example: 'my_username@example.com' or 'my_username')
try {
    val result : VideoChannel = apiInstance.videoChannelsChannelHandleGet(channelHandle)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#videoChannelsChannelHandleGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#videoChannelsChannelHandleGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoChannelsChannelHandlePut"></a>
# **videoChannelsChannelHandlePut**
> videoChannelsChannelHandlePut(channelHandle, videoChannelUpdate)

Update a video channel by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val channelHandle : kotlin.String = channelHandle_example // kotlin.String | The video channel handle (example: 'my_username@example.com' or 'my_username')
val videoChannelUpdate : VideoChannelUpdate =  // VideoChannelUpdate | 
try {
    apiInstance.videoChannelsChannelHandlePut(channelHandle, videoChannelUpdate)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#videoChannelsChannelHandlePut")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#videoChannelsChannelHandlePut")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |
 **videoChannelUpdate** | [**VideoChannelUpdate**](VideoChannelUpdate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="videoChannelsChannelHandleVideosGet"></a>
# **videoChannelsChannelHandleVideosGet**
> VideoListResponse videoChannelsChannelHandleVideosGet(channelHandle)

Get videos of a video channel by its id

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val channelHandle : kotlin.String = channelHandle_example // kotlin.String | The video channel handle (example: 'my_username@example.com' or 'my_username')
try {
    val result : VideoListResponse = apiInstance.videoChannelsChannelHandleVideosGet(channelHandle)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#videoChannelsChannelHandleVideosGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#videoChannelsChannelHandleVideosGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channelHandle** | **kotlin.String**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) |

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoChannelsGet"></a>
# **videoChannelsGet**
> kotlin.Array&lt;VideoChannel&gt; videoChannelsGet(start, count, sort)

Get list of video channels

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val start : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Offset
val count : java.math.BigDecimal = 8.14 // java.math.BigDecimal | Number of items
val sort : kotlin.String = sort_example // kotlin.String | Sort column (-createdAt for example)
try {
    val result : kotlin.Array<VideoChannel> = apiInstance.videoChannelsGet(start, count, sort)
    println(result)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#videoChannelsGet")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#videoChannelsGet")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **java.math.BigDecimal**| Offset | [optional]
 **count** | **java.math.BigDecimal**| Number of items | [optional]
 **sort** | **kotlin.String**| Sort column (-createdAt for example) | [optional]

### Return type

[**kotlin.Array&lt;VideoChannel&gt;**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="videoChannelsPost"></a>
# **videoChannelsPost**
> videoChannelsPost(videoChannelCreate)

Creates a video channel for the current user

### Example
```kotlin
// Import classes:
//import org.peertube.client.infrastructure.*
//import org.peertube.client.models.*

val apiInstance = VideoChannelApi()
val videoChannelCreate : VideoChannelCreate =  // VideoChannelCreate | 
try {
    apiInstance.videoChannelsPost(videoChannelCreate)
} catch (e: ClientException) {
    println("4xx response calling VideoChannelApi#videoChannelsPost")
    e.printStackTrace()
} catch (e: ServerException) {
    println("5xx response calling VideoChannelApi#videoChannelsPost")
    e.printStackTrace()
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videoChannelCreate** | [**VideoChannelCreate**](VideoChannelCreate.md)|  | [optional]

### Return type

null (empty response body)

### Authorization


Configure OAuth2:
    ApiClient.accessToken = ""

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

