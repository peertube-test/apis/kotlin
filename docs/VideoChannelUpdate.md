
# VideoChannelUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**displayName** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**support** | **kotlin.String** |  |  [optional]
**bulkVideosSupportUpdate** | **kotlin.Boolean** | Update all videos support field of this channel |  [optional]



