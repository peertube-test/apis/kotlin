
# CommentThreadResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**data** | [**kotlin.Array&lt;VideoComment&gt;**](VideoComment.md) |  |  [optional]



