
# ServerConfigCustom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**instance** | [**ServerConfigCustomInstance**](ServerConfigCustomInstance.md) |  |  [optional]
**theme** | [**ServerConfigCustomTheme**](ServerConfigCustomTheme.md) |  |  [optional]
**services** | [**ServerConfigCustomServices**](ServerConfigCustomServices.md) |  |  [optional]
**cache** | [**ServerConfigCustomCache**](ServerConfigCustomCache.md) |  |  [optional]
**signup** | [**ServerConfigCustomSignup**](ServerConfigCustomSignup.md) |  |  [optional]
**admin** | [**ServerConfigCustomAdmin**](ServerConfigCustomAdmin.md) |  |  [optional]
**contactForm** | [**ServerConfigEmail**](ServerConfigEmail.md) |  |  [optional]
**user** | [**ServerConfigUser**](ServerConfigUser.md) |  |  [optional]
**transcoding** | [**ServerConfigCustomTranscoding**](ServerConfigCustomTranscoding.md) |  |  [optional]
**import** | [**ServerConfigImport**](ServerConfigImport.md) |  |  [optional]
**autoBlacklist** | [**ServerConfigAutoBlacklist**](ServerConfigAutoBlacklist.md) |  |  [optional]
**followers** | [**ServerConfigCustomFollowers**](ServerConfigCustomFollowers.md) |  |  [optional]



