
# RegisterUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **kotlin.String** | The username of the user  | 
**password** | **kotlin.String** | The password of the user  | 
**email** | **kotlin.String** | The email of the user  | 
**displayName** | **kotlin.String** | The user display name |  [optional]
**channel** | [**RegisterUserChannel**](RegisterUserChannel.md) |  |  [optional]



