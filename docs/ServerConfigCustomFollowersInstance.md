
# ServerConfigCustomFollowersInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **kotlin.Boolean** |  |  [optional]
**manualApproval** | **kotlin.Boolean** |  |  [optional]



