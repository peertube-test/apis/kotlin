
# VideoPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**createdAt** | **kotlin.String** |  |  [optional]
**updatedAt** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]
**uuid** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**isLocal** | **kotlin.Boolean** |  |  [optional]
**videoLength** | [**java.math.BigDecimal**](java.math.BigDecimal.md) |  |  [optional]
**thumbnailPath** | **kotlin.String** |  |  [optional]
**privacy** | [**VideoPlaylistPrivacy**](VideoPlaylistPrivacy.md) |  |  [optional]
**type** | [**VideoPlaylistPrivacy**](VideoPlaylistPrivacy.md) |  |  [optional]
**ownerAccount** | [**VideoPlaylistOwnerAccount**](VideoPlaylistOwnerAccount.md) |  |  [optional]



