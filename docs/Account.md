
# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **kotlin.String** |  |  [optional]
**displayName** | **kotlin.String** |  |  [optional]
**description** | **kotlin.String** |  |  [optional]



